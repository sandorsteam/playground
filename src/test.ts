var assert = require('assert');


var strFilePath = "./testdata.csv"
var fs = require("fs");
var text = fs.readFileSync(strFilePath).toString();
var arrTextByLine = text.split("\n");
const separator = ','
var jsonString = ''
var dataRow = ''
var arrKeys: any;
arrTextByLine.forEach((textLine: string, index: number)=>{
  if(index == 0)
    arrKeys = textLine.split(separator);
  else{
    var arrData = textLine.split(separator);
    arrData.forEach((value: string, index: number)=>{
      if(index < arrData.length - 1)
      dataRow =  dataRow + '"' + arrKeys[index] + '":"' + value + '",'
      else
      dataRow =  dataRow + '"' + arrKeys[index] + '":"' + value + '"'
    })
    if(index < arrTextByLine.length - 1 )
    jsonString = jsonString + "{" + dataRow + "},"
    else
    jsonString = jsonString + "{" + dataRow + "}"
    dataRow=''
  } 
  
});
jsonString = "[" + jsonString + "]";

//debugger;

var myTestData = JSON.parse(jsonString);

function RunTest(data: any){
  //describe('teststepDesc'+index, async function() {
    
      it("teststep_" + data.a, async function() {
        //do the test here
        assert.equal([1, 2, 3].indexOf(4), -1);
      });


    //});
}


describe('Test Suite Name', async function() {
  
  before(async function() {
    //initial stuff

  });

  beforeEach(async function(){
    //console.log(" complete..")
  });
  
  myTestData.forEach( (testStep: object, index: number)=>{
    
    //describe('teststepDesc'+(index+1), async function() {
    RunTest(testStep);
      //debugger;
    //});
  });
    
});