const {Builder, By, Key, until} = require('selenium-webdriver');

var myfunc = async function(){
    let driver = new Builder().forBrowser('chrome').build();

    await driver.get("https://andreidbr.github.io/JS30/");

    await driver.findElement(By.xpath('/html/body/div[2]/div[1]')).click();

    await driver.wait(until.titleIs('JS30: 01 Drums'), 3000);
    
    await driver.close();
}

myfunc();