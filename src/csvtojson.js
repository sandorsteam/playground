/** csv file
a,b,c
1,2,3
4,5,6
*/
const csvFilePath='./testdata.csv';
const csv=require('csvtojson')
csv()
.fromFile(csvFilePath)
.then((jsonObjArr)=>{
    jsonObjArr.forEach((item)=>{
        console.log(item);
    });
    //console.log(jsonObjArr);
    /**
     * [
     * 	{a:"1", b:"2", c:"3"},
     * 	{a:"4", b:"5". c:"6"}
     * ]
     */ 
})
 
// Async / await usage
//const jsonArray=await csv().fromFile(csvFilePath);

var a=1;